# Core

Provides common functionality for websites in the *Relatively Irrelevant* project.

## Requirements

* PHP 5.5 or higher.
* MySQL 5.5 or higher (MariaDB is preferred).

## Installation

* TODO: Something about migrations.
* In the root directory, copy `core.default.json` to `core.json`. Then, adjust the configuration inside to `core.json` to suit your environment.
* Switch to the `.build` directory and run the following commands:
  * `composer update` to install PHP library dependencies.

Dependent projects will now function (after you have performed their own respective installation procedures).
