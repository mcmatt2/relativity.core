<?php namespace Relativity\Core;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Silex\Application;
use Silex\ServiceProviderInterface;

class CoreServiceProvider implements ServiceProviderInterface {
    /**
     * @see \Silex\ServiceProviderInterface::register
     *
     * @param Application $app
     */
    public function register(Application $app) {
        $app['relativity.core.entityManager'] = $app->share(function (Application $app) {
            /** @var array $config */
            $config = $app['relativity.core.config'];

            $setup = Setup::createAnnotationMetadataConfiguration([ __DIR__ . '/Entities' ], $config['debug'] ? : FALSE);

            $entityManager = EntityManager::create([
                'driver' => $config['database']['driver'] ? : 'pdo_mysql',
                'host' => $config['database']['host'] ? : '127.0.0.1',
                'user' => $config['database']['user'],
                'password' => $config['database']['pass'],
                'dbname' => $config['database']['name']
            ], $setup);

            return $entityManager;
        });
    }

    /**
     * @see \Silex\ServiceProviderInterface::boot
     *
     * @param Application $app
     */
    public function boot(Application $app) { }
}
