<?php namespace Relativity\Core\Traits;

/**
 * @package Relativity\Core\Traits
 *
 * Provides virtualisation for class properties, allowing you to access them
 * through getProperty(); and setProperty($value); magic methods.
 */
trait GetSet {
    public function __call($method, $args) {
        $start = \substr($method, 0, 3);
        $end = \substr($method, 3);
        $field = \lcfirst($end);

        if ( !empty($field) ) {
            if ( $start === 'get' ) {
                if ( !static::_GetSet_canRead($field) ) {
                    throw new \InvalidArgumentException("The field '{$field}' is not accessible.");
                }

                return $this->$field;
            }

            if ( $start === 'set' ) {
                if ( static::_GetSet_canRead($field) && !static::_GetSet_canWrite($field) ) {
                    throw new \InvalidArgumentException("The field '{$field}' is read-only.");
                }
                if ( !static::_GetSet_canWrite($field) ) {
                    throw new \InvalidArgumentException("The field '{$field}' is not writable.");
                }

                if ( $args ) {
                    $this->$field = \count($args) === 2 ? $args[1] : $args[0];
                } else {
                    $this->$field = NULL;
                }

                return $this;
            }
        }

        // For Twig, which translates $obj->name into $obj->name()
        $field = \lcfirst($method);
        if ( \property_exists($this, $field) ) {
            return $this->$field;
        }

        // We can't do anything with what we've been given. Throw out an error, and let the error handler handle it.
        $_class = \get_class($this);
        $_trace = \debug_backtrace();
        $_file = $_trace[0]['file'];
        $_line = $_trace[0]['line'];
        \trigger_error("Call to undefined method {$_class}::{$method}() in {$_file} on line {$_line}", \E_USER_ERROR);
    }

    private static function _GetSet_canRead($field) {
        return empty(static::$_GetSet) || \in_array($field, static::$_GetSet['read']);
    }

    private static function _GetSet_canWrite($field) {
        return empty(static::$_GetSet) || \in_array($field, static::$_GetSet['write']);
    }
}
