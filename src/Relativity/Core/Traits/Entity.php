<?php namespace Relativity\Core\Traits;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

/**
 * Adds a set of entity methods to a class.
 * @package Relativity\Core\Traits
 */
trait Entity {
    private static $_repositories = [ ];

    /**
     * Gets the repository for this entity class.
     *
     * @param EntityManager $manager
     *
     * @return EntityRepository The repository class.
     */
    private static function getRepository(EntityManager $manager) {
        $class = \get_called_class();
        if ( !\array_key_exists($class, self::$_repositories) ) {
            self::$_repositories[$class] = $manager->getRepository($class);
        }

        return self::$_repositories[$class];
    }

    /**
     * Finds an entity by its primary key / identifier.
     *
     * @param EntityManager $manager
     * @param mixed         $id The identifier.
     *
     * @return static The entity instance or NULL if the entity can not be found.
     */
    public static function find(EntityManager $manager, $id) {
        return static::getRepository($manager)->find($id);
    }

    /**
     * Finds all entities in the repository.
     *
     * @param EntityManager $manager
     *
     * @return static[] The entities.
     */
    public static function findAll(EntityManager $manager) {
        return static::getRepository($manager)->findAll();
    }

    /**
     * Finds entities by a set of criteria.
     *
     * @param EntityManager $manager
     * @param array         $criteria
     * @param array|null    $orderBy
     * @param int|null      $limit
     * @param int|null      $offset
     *
     * @return static[] The objects.
     */
    public static function findBy(EntityManager $manager, array $criteria = NULL, array $orderBy = NULL, $limit = NULL, $offset = NULL) {
        return static::getRepository($manager)->findBy($criteria ? : [ ], $orderBy, $limit, $offset);
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param EntityManager $manager
     * @param array         $criteria
     * @param array|null    $orderBy
     *
     * @return static The entity instance or NULL if the entity can not be found.
     */
    public static function findOneBy(EntityManager $manager, array $criteria, array $orderBy = NULL) {
        return static::getRepository($manager)->findOneBy($criteria, $orderBy);
    }
}
