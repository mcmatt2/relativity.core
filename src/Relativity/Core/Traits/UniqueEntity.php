<?php namespace Relativity\Core\Traits;

/**
 * Marks an entity as unique.
 * @package Relativity\Core\Traits
 */
trait UniqueEntity {
    /**
     * @var \int
     * @Id @GeneratedValue
     * @Column (
     *     name = "ID",
     *     type = "integer",
     *     unique = true
     * )
     */
    protected $id;

    public function getID() {
        return $this->id;
    }
}
