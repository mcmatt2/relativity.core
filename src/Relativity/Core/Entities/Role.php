<?php namespace Relativity\Core\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Relativity\Core\Traits\Entity;
use Relativity\Core\Traits\GetSet;
use Relativity\Core\Traits\UniqueEntity;

/**
 * @Entity
 *
 * @method string getName();
 *
 * @method static setName(\string $name);
 *
 * @method UserComicRole[] getUserComicRoles();
 */
class Role {
    use Entity, UniqueEntity, GetSet;

    public function __construct() {
        $this->userComicRoles = new ArrayCollection;
    }

    /**
     * @var \string
     * @Column (
     *     type = "string",
     *     length = 16,
     *     unique = true
     * )
     */
    protected $name;

#region Relationships
    /**
     * @var UserComicRole[]
     * @OneToMany (
     *     targetEntity = "UserComicRole",
     *     mappedBy = "role"
     * )
     */
    protected $userComicRoles;
#endregion
}
