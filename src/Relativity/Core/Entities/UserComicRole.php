<?php namespace Relativity\Core\Entities;

use Relativity\Core\Traits\Entity;
use Relativity\Core\Traits\GetSet;

/**
 * @Entity
 * @Table (
 *     name = "`User~Comic~Role`",
 *     uniqueConstraints = {
 *         @UniqueConstraint (
 *             name = "UX_UserID_ComicID_RoleID",
 *             columns = { "UserID", "ComicID", "RoleID" }
 *         )
 *     }
 * )
 */
class UserComicRole {
    use GetSet, Entity;

    public function __construct() { }

    /**
     * @var User
     * @Id
     * @Column (
     *     name = "UserID",
     *     type = "integer"
     * )
     * @ManyToOne (
     *     targetEntity = "User",
     *     inversedBy = "userComicRoles"
     * )
     * @JoinColumn (
     *     onDelete = "CASCADE"
     * )
     */
    protected $user;

    /**
     * @var Comic
     * @Id
     * @Column (
     *     name = "ComicID",
     *     type = "integer"
     * )
     * @ManyToOne (
     *     targetEntity = "Comic",
     *     inversedBy = "userComicRoles"
     * )
     * @JoinColumn (
     *     onDelete = "CASCADE"
     * )
     */
    protected $comic;

    /**
     * @var Role
     * @Id
     * @Column (
     *     name = "RoleID",
     *     type = "integer"
     * )
     * @ManyToOne (
     *     targetEntity = "Role",
     *     inversedBy = "roles"
     * )
     * @JoinColumn (
     *     onDelete = "CASCADE"
     * )
     */
    protected $role;
}
