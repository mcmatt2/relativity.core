<?php namespace Relativity\Core\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Relativity\Core\Traits\Entity;
use Relativity\Core\Traits\GetSet;
use Relativity\Core\Traits\UniqueEntity;
use Relativity\WritersDesk\Application;

/**
 * @Entity
 * @Table (
 *     name = "Chapter"
 * )
 *
 * @method Series  getSeries();
 * @method \string getName();
 * @method \string getPermalink();
 * @method \string getFullName();
 * @method \string getFullPermalink();
 * @method \string getTagline();
 * @method \bool   getIsHidden();
 * @method Comic[] getComics();
 *
 * @method static setTagline(Application $app, \string $tagline);
 * @method static setIsHidden(Application $app, \bool $isHidden);
 */
class Chapter {
    use GetSet, Entity, UniqueEntity;

    protected static $_GetSet = [
        'read' => [
            'series',
            'name',
            'permalink',
            'fullName',
            'fullPermalink',
            'tagline',
            'isHidden',
            'comics'
        ],
        'write' => [
            'tagline',
            'isHidden'
        ]
    ];

    public function __construct() {
        $this->comics = new ArrayCollection;
    }

    /**
     * @var Series
     * @ManyToOne (
     *     targetEntity = "Series",
     *     inversedBy = "chapters"
     * )
     * @JoinColumn (
     *     name = "SeriesID",
     *     referencedColumnName = "ID",
     *     onDelete = "SET NULL"
     * )
     */
    protected $series;

    /**
     * @var \string
     * @Column (
     *     name = "Name",
     *     type = "string",
     *     length = 64
     * )
     */
    protected $name;

    /**
     * @var \string
     * @Column (
     *     name = "Permalink",
     *     type = "string",
     *     length = 64
     * )
     */
    protected $permalink;

    /**
     * @var \string
     * @Column (
     *     name = "FullName",
     *     type = "string",
     *     length = 129
     * )
     */
    protected $fullName;

    /**
     * @var \string
     * @Column (
     *     name = "FullPermalink",
     *     type = "string",
     *     length = 129
     * )
     */
    protected $fullPermalink;

    /**
     * @var \string
     * @Column (
     *     name = "Tagline",
     *     type = "string",
     *     length = 128,
     *     nullable = true
     * )
     */
    protected $tagline = NULL;

    /**
     * @var \bool
     * @Column (
     *     name = "IsHidden",
     *     type = "boolean"
     * )
     */
    protected $isHidden = FALSE;

#region Relationships
    /**
     * @var Comic[]
     * @OneToMany (
     *     targetEntity = "Comic",
     *     mappedBy = "chapter"
     * )
     */
    protected $comics;

#endregion

    /**
     * Set the series of this chapter.
     *
     * @param Application $app
     * @param Series      $series
     * @param \bool       $updateLinks
     *
     * @return $this
     */
    public function setSeries(Application $app, Series $series, $updateLinks = TRUE) {
        $series->addChapter($this);
        $this->series = $series;

        if ( $updateLinks ) {
            $this->updateLinks($app);
        }

        return $this;
    }

    /**
     * Set the name of this chapter.
     *
     * @param Application $app
     * @param \string     $name
     * @param \bool       $updateLinks
     *
     * @return $this
     */
    public function setName(Application $app, $name, $updateLinks = TRUE) {
        $this->name = $name;
        $this->permalink = $app->urlify($name);

        if ( $updateLinks ) {
            $this->updateLinks($app);
        }

        return $this;
    }

    /**
     * Update the permanent links for this chapter.
     *
     * @param Application $app
     */
    private function updateLinks(Application $app) {
        if ( $this->series ) {
            $seriesName = $this->series->getName();
            $seriesPermalink = $this->series->getPermalink();
            $coalesced = ($this->name === $seriesName);

            $this->fullName = ($coalesced ? $this->name : "{$seriesName} {$this->name}");
            $this->fullPermalink = ($coalesced ? $this->permalink : "{$seriesPermalink}/{$this->permalink}");
        } else {
            $this->fullName = $this->name;
            $this->fullPermalink = $this->permalink;
        }
    }

    public function addComic(Comic $comic) {
        $this->comics[] = $comic;

        return $this;
    }
}
