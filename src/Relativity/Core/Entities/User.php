<?php namespace Relativity\Core\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Relativity\Core\Traits\Entity;
use Relativity\Core\Traits\GetSet;
use Relativity\Core\Traits\UniqueEntity;

/**
 * @Entity
 * @Table (
 *     name = "User",
 *     uniqueConstraints = {
 *         @UniqueConstraint (
 *             name = "UX_Username",
 *             columns = { "Username" }
 *         ),
 *         @UniqueConstraint (
 *             name = "UX_Email",
 *             columns = { "Email" }
 *         )
 *     },
 *     indexes = {
 *         @Index (
 *             name = "IX_Nickname", columns = { "Nickname" }
 *         )
 *     }
 * )
 *
 * @method string getUsername();
 * @method string getPassword();
 * @method string getEmail();
 * @method string getNickname();
 *
 * @method static setUsername(\string $username);
 * @method static setPassword(\string $password);
 * @method static setEmail(\string $email);
 * @method static setNickname(\string $nickname);
 *
 * @method Comic[] getComics();
 * @method UserComicRole[] getUserComicRoles();
 */
class User {
    use Entity, UniqueEntity, GetSet;

    public function __construct() {
        $this->userComicRoles = new ArrayCollection;
    }

    /**
     * @var \string
     * @Column (
     *     name = "Username",
     *     type = "string",
     *     length = 32
     * )
     */
    protected $username;

    /**
     * @var \string
     * @Column (
     *     name = "Password",
     *     type = "string",
     *     length = 64
     * )
     */
    protected $password;

    /**
     * @var \string
     * @Column (
     *     name = "Email",
     *     type = "string",
     *     length = 254,
     *     nullable = true
     * )
     */
    protected $email = NULL;

    /**
     * @var \string
     * @Column (
     *     name = "Nickname",
     *     type = "string",
     *     length = 64,
     *     nullable = true
     * )
     */
    protected $nickname = NULL;

#region Relationships
    /**
     * @var UserComicRole[]
     * @OneToMany (
     *     targetEntity = "UserComicRole",
     *     mappedBy = "user"
     * )
     */
    protected $userComicRoles;
#endregion

#region Virtuals
    /**
     * Retrieves the user's nickname (if they have one), or their username otherwise.
     *
     * @return \string
     */
    public function getName() {
        return $this->nickname ? : $this->username;
    }
#endregion
}
