<?php namespace Relativity\Core\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Relativity\Core\Traits\Entity;
use Relativity\Core\Traits\GetSet;
use Relativity\Core\Traits\UniqueEntity;
use Relativity\WritersDesk\Application;

/**
 * @Entity
 * @Table (
 *     name = "Tag"
 * )
 *
 * @method TagCategory getTagCategory();
 * @method \string     getName();
 * @method \string     getPermalink();
 * @method Comic[]     getComics();
 */
class Tag {
    use GetSet, Entity, UniqueEntity;

    protected static $_GetSet = [
        'read' => [
            'tagCategory',
            'name',
            'permalink',
            'comics'
        ],
        'write' => [
            'tagCategory',
            'name'
        ]
    ];

    public function __construct() {
        $this->comics = new ArrayCollection;
    }

    /**
     * @var TagCategory
     * @ManyToOne (
     *     targetEntity = "TagCategory",
     *     inversedBy = "tags"
     * )
     * @JoinColumn (
     *     name = "TagCategoryID",
     *     referencedColumnName = "ID",
     *     onDelete = "SET NULL"
     * )
     */
    protected $tagCategory;

    /**
     * @var \string
     * @Column (
     *     name = "Name",
     *     type = "string",
     *     length = 32
     * )
     */
    protected $name;

    /**
     * @var \string
     * @Column (
     *     name = "Permalink",
     *     type = "string",
     *     length = 32
     * )
     */
    protected $permalink;

    /**
     * @var Comic[]
     * @ManyToMany (
     *     targetEntity = "Comic",
     *     mappedBy = "tags"
     * )
     */
    protected $comics;

#region Setters
    public function setTagCategory(Application $app, TagCategory $tagCategory = NULL) {
        if ( $this->tagCategory ) {
            $this->tagCategory->removeTag($app, $this);
            $this->tagCategory = NULL;
        }

        if ( $tagCategory ) {
            $tagCategory->addTag($app, $this);
            $this->tagCategory = $tagCategory;
        }

        return $this;
    }

    public function setName(Application $app, $name) {
        $this->name = $name;
        $this->permalink = $app->urlify($name);

        return $this;
    }
#endregion

#region Comics
    public function addComic(Application $app, Comic $comic) {
        $comic->addTag($app, $this);
        $this->comics[] = $comic;

        return $this;
    }

    public function containsComic(Application $app, Comic $comic) {
        return $this->comics->contains($comic);
    }

    public function removeComic(Application $app, Comic $comic) {
        $this->comics->removeElement($comic);

        return $this;
    }
#endregion
}
