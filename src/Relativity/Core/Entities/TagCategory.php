<?php namespace Relativity\Core\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Relativity\Core\Traits\Entity;
use Relativity\Core\Traits\GetSet;
use Relativity\Core\Traits\UniqueEntity;
use Relativity\WritersDesk\Application;

/**
 * @Entity
 * @Table (
 *     name = "TagCategory",
 *     uniqueConstraints = {
 *         @UniqueConstraint ( name = "UX_Name", columns = { "Name" } ),
 *         @UniqueConstraint ( name = "UX_Permalink", columns = { "Permalink" } )
 *     }
 * )
 *
 * @method \string getName();
 * @method \string getPermalink();
 * @method \int    getIndex();
 * @method Tag[]   getTags();
 */
class TagCategory {
    use GetSet, Entity, UniqueEntity;

    protected static $_GetSet = [
        'read' => [
            'name',
            'permalink',
            'index',
            'tags'
        ],
        'write' => [
            'name',
            'index'
        ]
    ];

    public function __construct() {
        $this->tags = new ArrayCollection;
    }

    /**
     * @var \string
     * @Column (
     *     name = "Name",
     *     type = "string",
     *     length = 32
     * )
     */
    protected $name;

    /**
     * @var \string
     * @Column (
     *     name = "Permalink",
     *     type = "string",
     *     length = 32
     * )
     */
    protected $permalink;

    /**
     * @var \int
     * @Column (
     *     name = "Idx",
     *     type = "integer"
     * )
     */
    protected $index = 0;

    /**
     * @var Tag[]
     * @OneToMany (
     *     targetEntity = "Tag",
     *     mappedBy = "tagCategory"
     * )
     */
    protected $tags;

#region Setters
    public function setName(Application $app, $name) {
        $this->name = $name;
        $this->permalink = $app->urlify($name);

        return $this;
    }
#endregion

#region Tags
    public function addTag(Application $app, Tag $tag) {
        $this->tags[] = $tag;

        return $this;
    }

    public function containsTag(Application $app, Tag $tag) {
        return $this->tags->contains($tag);
    }

    public function removeTag(Application $app, Tag $tag) {
        $this->tags->removeElement($tag);

        return $this;
    }
#endregion
}
