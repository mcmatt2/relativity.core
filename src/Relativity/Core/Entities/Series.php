<?php namespace Relativity\Core\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Relativity\Core\Traits\Entity;
use Relativity\Core\Traits\GetSet;
use Relativity\Core\Traits\UniqueEntity;
use Relativity\WritersDesk\Application;

/**
 * @Entity
 * @Table (
 *     name = "Series",
 *     uniqueConstraints = {
 *         @UniqueConstraint ( name = "UX_Name", columns = { "Name" } ),
 *         @UniqueConstraint ( name = "UX_Permalink", columns = { "Permalink" } )
 *     }
 * )
 *
 * @method \string   getName();
 * @method \string   getPermalink();
 * @method \string   getTagline();
 * @method \bool     getIsHidden();
 * @method Chapter[] getChapters();
 *
 * @method static setTagline(Application $app, \string $tagline);
 * @method static setIsHidden(Application $app, \bool $isHidden);
 */
class Series {
    use GetSet, Entity, UniqueEntity;

    protected static $_GetSet = [
        'read' => [
            'name',
            'permalink',
            'tagline',
            'isHidden',
            'chapters'
        ],
        'write' => [
            'tagline',
            'isHidden'
        ]
    ];

    public function __construct() {
        $this->chapters = new ArrayCollection;
    }

    /**
     * @var \string
     * @Column (
     *     name = "Name",
     *     type = "string",
     *     length = 64
     * )
     */
    protected $name;

    /**
     * @var \string
     * @Column (
     *     name = "Permalink",
     *     type = "string",
     *     length = 64
     * )
     */
    protected $permalink;

    /**
     * @var \string
     * @Column (
     *     name = "Tagline",
     *     type = "string",
     *     length = 128,
     *     nullable = true
     * )
     */
    protected $tagline = NULL;

    /**
     * @var \bool
     * @Column (
     *     name = "IsHidden",
     *     type = "boolean"
     * )
     */
    protected $isHidden = FALSE;

#region Relationships
    /**
     * @var Chapter[]
     * @OnetoMany (
     *     targetEntity = "Chapter",
     *     mappedBy = "series"
     * )
     */
    protected $chapters;

#endregion

    /**
     * Set the name of this series.
     *
     * @param Application $app
     * @param \string     $name
     *
     * @return $this
     */
    public function setName(Application $app, $name) {
        $this->name = $name;
        $this->permalink = $app->urlify($name);

        return $this;
    }

    public function addChapter(Chapter $chapter) {
        $this->chapters[] = $chapter;

        return $this;
    }
}
