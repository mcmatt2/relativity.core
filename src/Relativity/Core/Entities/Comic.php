<?php namespace Relativity\Core\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Relativity\Core\Traits\Entity;
use Relativity\Core\Traits\GetSet;
use Relativity\Core\Traits\UniqueEntity;
use Relativity\WritersDesk\Application;

/**
 * @Entity
 * @Table (
 *     name = "Comic"
 * )
 *
 * @method Chapter getChapter();
 * @method \string getName();
 * @method \string getPermalink();
 * @method \string getFullName();
 * @method \string getFullPermalink();
 * @method \bool   getIsHidden();
 * @method \bool   getIsLocked();
 *
 * @method static setIsHidden(Application $app, \bool $isHidden);
 * @method static setIsLocked(Application $app, \bool $isLocked);
 */
class Comic {
    use GetSet, Entity, UniqueEntity;

    protected static $_GetSet = [
        'read' => [
            'chapter',
            'name',
            'permalink',
            'fullName',
            'fullPermalink',
            'isHidden',
            'isLocked'
        ],
        'write' => [
            // 'chapter' and 'name' are handled separately.
            'isHidden',
            'isLocked'
        ]
    ];

    public function __construct() {
        $this->tags = new ArrayCollection;
        $this->userComicRoles = new ArrayCollection;
    }

    /**
     * @var Chapter
     * @ManyToOne (
     *     targetEntity = "Chapter",
     *     inversedBy = "comics"
     * )
     * @JoinColumn (
     *     name = "ChapterID",
     *     referencedColumnName = "ID",
     *     onDelete = "SET NULL"
     * )
     */
    protected $chapter;

    /**
     * @var \string
     * @Column (
     *     name = "Name",
     *     type = "string",
     *     length = 64
     * )
     */
    protected $name;

    /**
     * @var \string
     * @Column (
     *     name = "Permalink",
     *     type = "string",
     *     length = 64
     * )
     */
    protected $permalink;

    /**
     * @var \string
     * @Column (
     *     name = "FullName",
     *     type = "string",
     *     length = 194
     * )
     */
    protected $fullName;

    /**
     * @var \string
     * @Column (
     *     name = "FullPermalink",
     *     type = "string",
     *     length = 194
     * )
     */
    protected $fullPermalink;

    /**
     * @var \bool
     * @Column (
     *     name = "IsHidden",
     *     type = "boolean"
     * )
     */
    protected $isHidden = FALSE;

    /**
     * @var \bool
     * @Column (
     *     name = "IsLocked",
     *     type = "boolean"
     * )
     */
    protected $isLocked = FALSE;

#region Relationships
    /**
     * @var UserComicRole[]
     * @OneToMany (
     *     targetEntity = "UserComicRole",
     *     mappedBy = "comic"
     * )
     */
    protected $userComicRoles;

    /**
     * @var Tag[]
     * @ManyToMany (
     *     targetEntity = "Tag",
     *     inversedBy = "comics"
     * )
     * @JoinTable (
     *     name = "`Comic~Tag`"
     * )
     */
    protected $tags;

#endregion


    /**
     * Set the chapter of this comic.
     *
     * @param Application $app
     * @param Chapter     $chapter
     * @param \bool       $updateLinks
     *
     * @return $this
     */
    public function setChapter(Application $app, Chapter $chapter, $updateLinks = TRUE) {
        $chapter->addComic($this);
        $this->chapter = $chapter;

        if ( $updateLinks ) {
            $this->updateLinks($app);
        }

        return $this;
    }

    /**
     * Set the name of this comic.
     *
     * @param Application $app
     * @param \string     $name
     * @param \bool       $updateLinks
     *
     * @return $this
     */
    public function setName(Application $app, $name, $updateLinks = TRUE) {
        $this->name = $name;
        $this->permalink = $app->urlify($name);

        if ( $updateLinks ) {
            $this->updateLinks($app);
        }

        return $this;
    }

    /**
     * Update the permanent links for this comic.
     *
     * @param Application $app
     */
    private function updateLinks(Application $app) {
        if ( $this->chapter ) {
            $chapterName = $this->chapter->getFullName();
            $chapterPermalink = $this->chapter->getFullPermalink();
            $coalesced = ($this->name === $chapterName);

            $this->fullName = ($coalesced ? $this->name : "{$chapterName} ({$this->name})");
            $this->fullPermalink = ($coalesced ? $this->permalink : "{$chapterPermalink}/{$this->permalink}");
        } else {
            $this->fullName = $this->name;
            $this->fullPermalink = $this->permalink;
        }
    }

    public function addTag(Application $app, Tag $tag) {
        $this->tags[] = $tag;

        return $this;
    }
}
